
const request = require('supertest');
const expect = require('chai').expect;

describe('Get API tests using supertest and Mocha Framework', () => {
	const baseurl = 'https://reqres.in';
	
	it('should successfully pass the test for get Filter out list of First name of all the Users', (done) => {
		
		request(baseurl)
			.get('/api/users/')
			.end(function (err, res) {
				expect(res.statusCode).to.be.equal(200);
				done();
				const firstName = res.body.data.map(item => item.first_name);
        		console.log('List of First_names from data : '+firstName);
			});
	});

	it('should successfully pass the test for get Filter out list of Email ID of all the Users', (done) => {
		request(baseurl)
			.get('/api/users/')
			.end(function (err, res) {
				expect(res.statusCode).to.be.equal(200);
				done();
				const emailIDs = res.body.data.map(item => item.email);
        		console.log('List of Email Ids : '+emailIDs);
			});
	});
	


	it('should fetch user details by ID', (done) => {
		const userId = 2; // Replace with the desired user ID
		request(baseurl)
			.get('/api/users/')
			.end(function (err, response) {
				expect(response.statusCode).to.be.equal(200);
				done();
				const filteredID = response.body.data.map(item => item.id).filter(id => id === userId);
				const filteredEmailID = response.body.data.map(item => item.email).filter(email => email === 'janet.weaver@reqres.in');
				console.log("ID: "+filteredID);
				console.log("Email ID: "+filteredEmailID);
	  });
	});
});
